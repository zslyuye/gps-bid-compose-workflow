package com.sd365.workflow.pojo.query;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Table;

@ApiModel(value="com.sd365.permission.centre.entity.Unit")
@Table(name = "basic_unit")
public class UnitQuery extends BaseEntity {
    /**
     * 公司名
     */
    @ApiModelProperty(value="name单位名")
    private String name;

    /**
     * 公司编号
     */
    @ApiModelProperty(value="code单位编号")
    private String code;

    /**
     * 助记码
     */
    @ApiModelProperty(value="mnemonicCode助记码")
    private String mnemonicCode;

    /**
     * 法人
     */
    @ApiModelProperty(value="master法人")
    private String master;

    /**
     * 税号
     */
    @ApiModelProperty(value="tax税号")
    private String tax;

    /**
     * 传真
     */
    @ApiModelProperty(value="fax传真")
    private String fax;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 地址
     */
    @ApiModelProperty(value="address地址")
    private String address;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 网址
     */
    @ApiModelProperty(value="website网址")
    private String website;

    @ApiModelProperty(value="modifier修改者")
    private String modifier;

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @ApiModelProperty(value="creator创建者")
    private String creator;

    @ApiModelProperty(value="orgId组织")
    private Long zoneId;

    @ApiModelProperty(value="tenantId租户")
    private Long tenantId;

    /**
     * 公司名
     */
    public String getName() {
        return name;
    }

    /**
     * 公司名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 公司编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 公司编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 助记码
     */
    public String getMnemonicCode() {
        return mnemonicCode;
    }

    /**
     * 助记码
     */
    public void setMnemonicCode(String mnemonicCode) {
        this.mnemonicCode = mnemonicCode;
    }

    /**
     * 法人
     */
    public String getMaster() {
        return master;
    }

    /**
     * 法人
     */
    public void setMaster(String master) {
        this.master = master;
    }

    /**
     * 税号
     */
    public String getTax() {
        return tax;
    }

    /**
     * 税号
     */
    public void setTax(String tax) {
        this.tax = tax;
    }

    /**
     * 传真
     */
    public String getFax() {
        return fax;
    }

    /**
     * 传真
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * 电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 网址
     */
    public String getWebsite() {
        return website;
    }

    /**
     * 网址
     */
    public void setWebsite(String website) {
        this.website = website;
    }


    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}