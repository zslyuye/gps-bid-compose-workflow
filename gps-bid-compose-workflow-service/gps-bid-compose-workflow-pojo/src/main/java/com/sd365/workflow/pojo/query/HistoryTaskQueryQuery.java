package com.sd365.workflow.pojo.query;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-31 00:00
 */
public class HistoryTaskQueryQuery {
    /* 业务标识 key */
    String businessKey;
    /* 任务 id */
    String taskId;
    /* 任务 名称*/
    String name;
    /* 状态 ： 1：正常前进，2：退回*/
    String status;
    /* 任务 定义key：就是任务的id */
    String taskDefinitionKey;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
