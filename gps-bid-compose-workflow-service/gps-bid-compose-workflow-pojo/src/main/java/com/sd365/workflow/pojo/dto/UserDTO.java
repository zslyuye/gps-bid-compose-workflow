package com.sd365.workflow.pojo.dto;

import lombok.Data;

@Data
public class UserDTO {
    /**
     * 代码， 不可 重复
     */
    private String code;
    /**
     * 用户名称
     */
    private String name;
    /**
     * 角色
     */
    private RoleDTO roleDTO;
    /**
     * 区划
     */
    private ZoneDTO zone;
    /**
     * 单位
     */
    private UnitDTO unit;
}