package com.sd365.workflow.pojo.dto;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-31 00:00
 */
public class HistoryTaskQueryDTO {
    /* 业务标识 key */
    String businessKey;
    /* 任务 id */
    String taskId;
    /* 任务 名称*/
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
