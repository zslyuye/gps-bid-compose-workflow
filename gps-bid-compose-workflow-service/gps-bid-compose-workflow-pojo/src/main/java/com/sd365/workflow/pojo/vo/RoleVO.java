package com.sd365.workflow.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

@Data
@ApiModel(value="com.sd365.workflow.dao.entity.Role")
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_role")
public class RoleVO extends TenantBaseEntity {
    /**
     * 角色名
     */
    @ApiModelProperty(value="name角色名")
    private String name;

    /**
     * 角色代码
     */
    @ApiModelProperty(value="code角色代码")
    private String code;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 预计到达时间
     */
    @ApiModelProperty(value="predictReachTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createdTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value="updateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updatedTime;

    /**
     * 单位
     */
    @ApiModelProperty(value="unit")
    private UnitVO unit;

    /**
     * 区划
     */
    @ApiModelProperty(value="zone")
    private ZoneVO zone;

}