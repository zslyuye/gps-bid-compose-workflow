package com.sd365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:05
 */
@EnableSwagger2
@SpringBootApplication
@MapperScan(("com.sd365.workflow.dao.mapper"))
@ComponentScans({@ComponentScan("com.sd365.common"), @ComponentScan("com.sd365.common.api.version")})
@EnableDiscoveryClient
public class WorkflowApp {
    public static void main(String[] args) {
        SpringApplication.run(WorkflowApp.class, args);
    }
}
