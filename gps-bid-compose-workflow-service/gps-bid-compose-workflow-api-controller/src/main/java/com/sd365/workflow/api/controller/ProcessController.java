package com.sd365.workflow.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.workflow.api.ProcessApi;
import com.sd365.workflow.pojo.dto.TaskQueryDTO;
import com.sd365.workflow.pojo.vo.TaskQueryVO;
import com.sd365.workflow.service.ProcessService;
import com.sd365.workflow.service.exception.WorkFlowException;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:04
 */
@Slf4j
@RestController
public class ProcessController implements ProcessApi {

    /* 流程服务 */
    private ProcessService processService;

    @Autowired
    public ProcessController(ProcessService processService) {
        this.processService = processService;
    }

    /**
     * jy : 部署流程
     *
     * @param processName 流程名称
     * @return TaskQueryDTO
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public Boolean deployProcess(@RequestParam("processName") String processName, @RequestParam("resourcePath") String resourcePath) {
        Boolean deployFlag;
        try {
            deployFlag = processService.deployment(processName, resourcePath);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_DEPLOY_PROCESS_EXCEPTION, e);
        }
        return deployFlag;
    }

    /**
     * jy : 启动流程
     *
     * @param processDefinitionKey 流程定义 key
     * @param businessKey          业务标识 key
     * @param assignee             启动流程负责人
     * @return boolean 成功返回 true
     * @author jy
     * @date 21:29 2020/12/28
     */
    @ApiLog
    @Override
    public TaskQueryVO startProcess(String processDefinitionKey,
                                    String businessKey,
                                    String assignee,
                                    String zone,
                                    String unit,
                                    Integer status) {
        ProcessInstance processInstance;
        try {
            processInstance = processService.start(processDefinitionKey, businessKey, assignee, zone, unit, status);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_START_PROCESS_INSTANCE_EXCEPTION, e);
        }
        TaskQueryDTO taskQueryDTO = new TaskQueryDTO();
        taskQueryDTO.setBusinessKey(processInstance.getBusinessKey());
        taskQueryDTO.setTaskId("start_task");
        taskQueryDTO.setName("启动流程");
        TaskQueryVO taskQueryVO;
        try {
            taskQueryVO = BeanUtil.copy(taskQueryDTO, TaskQueryVO.class);
        } catch (Exception e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return taskQueryVO;
    }

    /**
     * jy : 查询代办流程
     *
     * @param processDefinitionKey 流程定义 key
     * @param candidateUser        候选人
     * @return java.util.List<org.activiti.engine.task.Task>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public List<TaskQueryDTO> queryCandidateTask(String processDefinitionKey,
                                                 String taskDefinitionKey,
                                                 String candidateUser) {
        try {
            return processService.queryTask(processDefinitionKey, taskDefinitionKey, candidateUser);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_FIND_CANDIDATE_TASK_EXCEPTION, e);
        }
    }


    /**
     * jy : 查询  负责人 代办流程
     *
     * @param processDefinitionKey 流程定义 key
     * @param taskDefinitionKey    任务定义key
     * @param assignee             负责人
     * @return List<TaskQueryDTO>  任务DTO对象
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public List<TaskQueryDTO> queryAssigneeTask(String processDefinitionKey,
                                                String taskDefinitionKey,
                                                String assignee) {
        try {
            return processService.queryAssigneeTask(processDefinitionKey, taskDefinitionKey, assignee);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_START_PROCESS_INSTANCE_EXCEPTION, e);
        }
    }

    /**
     * * 完成任务
     * <p>
     * * @param taskId 任务 id
     * * @param assignee   负责人
     * * @return boolean
     *
     * @param taskId   任务id
     * @param assignee 负责人
     * @param status   状态 ：1 ：正常进行， 2 ：退回 （有排他网关的是这个流程，没有网关的，统一填1）
     * @return Boolean 成功 就是true ，失败就是 false
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public Boolean completeTask(String taskId,
                                String assignee,
                                Integer status) {
        try {
            return processService.completeTask(taskId, assignee, status);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_COMPILE_TASK_FAIL_EXCEPTION, e);
        }
    }

    /**
     * 拾取任务
     *
     * @param taskId        任务 id
     * @param candidateUser 用户
     * @return java.lang.Boolean
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public Boolean claimTask(String taskId,
                             String candidateUser) {
        try {
            return processService.claimTask(taskId, candidateUser);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_CLAIM_TASK_FAIL_EXCEPTION, e);
        }
    }


    /**
     * 结束 流程
     *
     * @param processInstanceId 流程实例 id
     * @return java.lang.Boolean
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public Boolean deleteDeployProcess(String processInstanceId) {
        try {
            return processService.deleteDeployProcess(processInstanceId);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_DELETE_PROCESS_INSTANCE_EXCEPTION, e);
        }
    }

}
