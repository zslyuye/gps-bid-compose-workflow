package com.sd365.workflow.api.controller;

import com.sd365.workflow.api.ResourceApi;
import com.sd365.workflow.pojo.vo.HistoryTaskQueryVO;
import com.sd365.workflow.util.FileUtils;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RestController
public class ResourceController implements ResourceApi {

    @Resource
    RepositoryService repositoryService;


    @Override
    public void getBpmn(String processDefinitionKey, String assignee) throws IOException, IOException {
        // 1、得到引擎

        // 2、获取repositoryService
        // 3、得到查询器：ProcessDefinitionQuery，设置查询条件,得到想要的流程定义
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("myEvection")
                .singleResult();
        // 4、通过流程定义信息，得到部署ID
        String deploymentId = processDefinition.getDeploymentId();
        // 5、通过repositoryService的方法，实现读取图片信息和bpmn信息
        // png图片的流
        InputStream pngInput = repositoryService.getResourceAsStream(deploymentId, processDefinition.getDiagramResourceName());
        // bpmn文件的流
        InputStream bpmnInput = repositoryService.getResourceAsStream(deploymentId, processDefinition.getResourceName());
        // 6、构造OutputStream流
        File file_png = new File("d:/evectionflow01.png");
        File file_bpmn = new File("d:/evectionflow01.bpmn");
        FileOutputStream bpmnOut = new FileOutputStream(file_bpmn);
        FileOutputStream pngOut = new FileOutputStream(file_png);
        // 7、输入流，输出流的转换
        IOUtils.copy(pngInput, pngOut);
        IOUtils.copy(bpmnInput, bpmnOut);
        // 8、关闭流
        pngOut.close();
        bpmnOut.close();
        pngInput.close();
        bpmnInput.close();

    }

    @Override
    public String downloadData(HttpServletResponse res) {
        String data = "这是一个下载文件";   //传入数据
        File file = new File("bpmn/bidCompose.bpmn");
        FileUtils.getFile(data.getBytes(), file.getName());
        FileUtils.responseTo(file, res);
        file.delete();
        System.out.println("success");
        return "success";
    }

}
