package com.sd365.workflow.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.workflow.api.HistoryApi;
import com.sd365.workflow.pojo.dto.HistoryTaskQueryDTO;
import com.sd365.workflow.pojo.vo.HistoryTaskQueryVO;
import com.sd365.workflow.service.HistoryProcessService;
import com.sd365.workflow.service.exception.WorkFlowException;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@Slf4j
@RestController
public class HistoryController implements HistoryApi {

    @Resource
    HistoryProcessService historyProcessService; // 历史服务

    // 历史 查询 list dto 对象
    List<HistoryTaskQueryDTO> historyTaskQueryDTOS = null;

    List<HistoryTaskQueryVO> historyTaskQueryVOS = null;

    /**
     * jy 查询当前负责人已完成的历史任务
     *
     * @param assignee 负责人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public List<HistoryTaskQueryVO> queryAssigneeFinishHistoryTask(String processDefinitionKey, String assignee) {
        try {
            historyTaskQueryDTOS = historyProcessService.queryAssigneeFinishHistoryTask(processDefinitionKey, assignee);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        try {
            historyTaskQueryVOS = BeanUtil.copyList(historyTaskQueryDTOS, HistoryTaskQueryVO.class);
        } catch (Exception e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return historyTaskQueryVOS;
    }

    /**
     * jy 查询当前负责人未完成的历史任务
     *
     * @param assignee 负责人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public List<HistoryTaskQueryVO> queryAssigneeUnFinishHistoryTask(String processDefinitionKey, String assignee) {
        try {
            historyTaskQueryDTOS = historyProcessService.queryAssigneeUnFinishHistoryTask(processDefinitionKey, assignee);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        try {
            historyTaskQueryVOS = BeanUtil.copyList(historyTaskQueryDTOS, HistoryTaskQueryVO.class);
        } catch (Exception e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return historyTaskQueryVOS;
    }

    /**
     * jy 查询候选人 的 历史任务
     *
     * @param candidateUser 候选人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public List<HistoryTaskQueryVO> queryCandidateFinishHistoryTask(String processDefinitionKey, String candidateUser) {
        try {
            historyTaskQueryDTOS = historyProcessService.queryCandidateFinishHistoryTask(processDefinitionKey, candidateUser);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }

        try {
            historyTaskQueryVOS = BeanUtil.copyList(historyTaskQueryDTOS, HistoryTaskQueryVO.class);
        } catch (Exception e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return historyTaskQueryVOS;
    }

    /**
     * jy 查询候选人 的 历史任务
     *
     * @param candidateUser 候选人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiLog
    @Override
    public List<HistoryTaskQueryVO> queryCandidateUnFinishHistoryTask(String processDefinitionKey, String candidateUser) {
        List<HistoryTaskQueryDTO> historyTaskQueryDTOS;
        try {
            historyTaskQueryDTOS = historyProcessService.queryCandidateUnFinishHistoryTask(processDefinitionKey, candidateUser);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        try {
            historyTaskQueryVOS = BeanUtil.copyList(historyTaskQueryDTOS, HistoryTaskQueryVO.class);
        } catch (Exception e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        return historyTaskQueryVOS;
    }

}
