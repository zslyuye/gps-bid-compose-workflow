package com.sd365.workflow.service;

import com.sd365.workflow.pojo.dto.HistoryTaskQueryDTO;
import com.sd365.workflow.pojo.dto.TaskQueryDTO;
import org.activiti.engine.history.HistoricTaskInstance;

import java.util.List;

public interface HistoryProcessService {


    /**
     * jy : 查询 负责人的 已完成的历史任务
     * @param assignee 负责人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<HistoryTaskQueryDTO> queryAssigneeFinishHistoryTask(String processDefinitionKey,String assignee);

    /**
     * jy : 查询 负责人的 未完成的历史任务
     * @param assignee 负责人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<HistoryTaskQueryDTO> queryAssigneeUnFinishHistoryTask(String processDefinitionKey,String assignee);


    /**
     * jy : 查询 候选人的 已完成的历史任务
     * @param candidate 候选人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<HistoryTaskQueryDTO> queryCandidateFinishHistoryTask(String processDefinitionKey,String candidate);


    /**
     * jy : 查询 负责人的 未完成的历史任务
     * @param candidate 候选人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<HistoryTaskQueryDTO> queryCandidateUnFinishHistoryTask(String processDefinitionKey,String candidate);


    /**
     *  特殊方法 :  把 HistoricTaskInstance 转化为 DTO
     * @param instanceList instanceList
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<HistoryTaskQueryDTO> changeInstanceObjToDTO(List<HistoricTaskInstance> instanceList);


}
