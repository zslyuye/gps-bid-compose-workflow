package com.sd365.workflow.service;

import com.sd365.workflow.pojo.dto.TaskQueryDTO;
import org.activiti.engine.runtime.ProcessInstance;

import java.util.List;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:19
 */
public interface ProcessService {

    /**
     * 流程部署
     *
     * @param processName 流程名
     * @param resource    流程文件路径
     * @return void
     * @author Yan Huazhi
     * @date 22:42 2020/12/28
     */
    Boolean deployment(String processName, String resource);

    /**
     * 启动流程服务
     *
     * @param processDefinitionKey 流程定义 key
     * @param businessKey          业务标识 key
     * @param assignee             启动流程用户 id
     * @param zone                 区划
     * @param unit                 单位
     * @return ProcessInstance 流程实例
     * @author jy
     * @date 13:57 2020/12/30
     */
    ProcessInstance start(String processDefinitionKey, String businessKey, String assignee, String zone, String unit, Integer status);

    /**
     * 根据流程定义 key 和 用户 id 查询对应用户对应流程代办列表
     *
     * @param processDefinitionKey 流程定义 key
     * @param candidateUser        候选人
     * @return List<Task> 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<TaskQueryDTO> queryTask(String processDefinitionKey, String taskDefinitionKey, String candidateUser);

    /**
     * 完成任务
     *
     * @param taskId   任务 id
     * @param assignee 负责人
     * @return boolean
     * @author jy
     * @date 13:57 2020/12/30
     */
    Boolean completeTask(String taskId, String assignee,Integer status);

    /**
     * 拾取任务
     *
     * @param candidateUser 用户
     * @param taskId        任务 id
     * @return boolean 成功返回 true
     * @author jy
     * @date 13:57 2020/12/30
     */
    Boolean claimTask(String taskId, String candidateUser);

    /**
     * jy : 删除 部署的 流程
     *
     * @param processInstanceId 流程实力 id
     * @return 成功 true， 失败 false
     * @author jy
     * @date 13:57 2020/12/30
     */
    Boolean deleteDeployProcess(String processInstanceId);

    /**
     * 根据流程定义 key 和 用户 id 查询对应  负责人 对应流程代办列表
     *
     * @param processDefinitionKey 流程定义 key
     * @param assignee             负责人
     * @return List<Task> 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<TaskQueryDTO> queryAssigneeTask(String processDefinitionKey, String taskDefinitionKey, String assignee);
}
