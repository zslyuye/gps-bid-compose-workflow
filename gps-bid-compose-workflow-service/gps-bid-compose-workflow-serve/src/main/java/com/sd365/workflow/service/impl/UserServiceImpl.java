package com.sd365.workflow.service.impl;

import com.sd365.workflow.dao.entity.User;
import com.sd365.workflow.dao.mapper.UserMapper;
import com.sd365.workflow.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:20
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    public List<String> findUserList(Long zoneId, Long unitId, Long roleId) {
        return null;
    }

    @Resource
    UserMapper userMapper;

    /**
     * 根据用户 id 查询用户的组
     * @param user user
     * @return  List<String>
     */
    @Override
    public List<String> queryGroupByUserId(String user) {
        List<String> groupList = new LinkedList<String>();
        if (user.equals("经理1.1") || user.equals("经理1.2")) {
            // 经理1 组
            groupList.add("经理1");
        } else if (user.equals("经理2.1")) {
            // 经理2 组
            groupList.add("经理2");
        } else if (user.equals("总经理")) {
            // 总经理 组
            groupList.add("总经理");
        } else if (user.equals("1")) {
            groupList.add("其他组");
        }
        return groupList;
    }

    /**
     * 根据 id 查询上级
     *
     * @param taskDefinitionKey 任务 key
     * @param assignee 负责人
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author Yan Huazhi
     * @date 23:25 2020/12/30
     */
    @Override
    public Map<String, Object> queryVariables(String taskDefinitionKey, String assignee) {
        HashMap<String, Object> variables = new HashMap<>();
        // 经理1和经理2组的 上为 总经理
        if (assignee.equals("经理1.1") || assignee.equals("经理1.2") || assignee.equals("经理2.1")) {
            variables.put("group", "总经理");
        } else {
            // 其他上级都是经理1
            variables.put("group", "经理1");
        }
        return variables;
    }

    /**
     * 通过 区划， 单位， 角色， 定位候选人组
     *
     * @param zone 区划
     * @param unit 单位
     * @param role 角色
     * @return 定位候选人组
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<String> getCandidateByZoneUnitRole(String zone, String unit, String role) {
        List<String> candidateList = new ArrayList<>();
        List<User> users = userMapper.selectCandidateByZoneUnitRole(role, unit, zone);
        for (User user : users){
                candidateList.add(user.getCode());
                log.info("user的code " + user.getCode());
                log.info("user的name " + user.getName());
                log.info("user的zone " + user.getZone().getName());
        }
        return candidateList;
    }


    /**
     * Moke
     * 查询政府采购的候选人
     * 通过 区划， 单位， 角色， 定位候选人组
     *
     * @param zone 区划
     * @param unit 单位
     * @param role 角色
     * @return 定位候选人组
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<String> getCandidateWithGovernment(String zone, String unit, String role) {
        ArrayList<String> governmentCandidateList = new ArrayList<>();
        governmentCandidateList.add("government1");
        governmentCandidateList.add("government2");
        governmentCandidateList.add("government3");
        governmentCandidateList.add("government4");
        governmentCandidateList.add("government5");
        log.info("++++++++++++++++++++++ 政府采购部门的 候选人");
        System.out.println(governmentCandidateList);
        return governmentCandidateList;
    }

    /**
     * Moke DATA
     * 查询代理机构的候选人
     * 通过 区划， 单位， 角色， 定位候选人组
     *
     * @param zone 区划
     * @param unit 单位
     * @param role 角色
     * @return 定位候选人组
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<String> getCandidateWithAgent(String zone, String unit, String role) {
        ArrayList<String> agentCandidate = new ArrayList<>();
        agentCandidate.add("agent1");
        agentCandidate.add("agent2");
        agentCandidate.add("agent3");
        agentCandidate.add("agent4");
        agentCandidate.add("agent5");
        log.info("++++++++++++++++++++++ 代理机构的的 候选人");
        System.out.println(agentCandidate);
        return agentCandidate;
    }


}
