package com.sd365.workflow.service.impl;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.workflow.pojo.dto.HistoryTaskQueryDTO;
import com.sd365.workflow.service.HistoryProcessService;
import com.sd365.workflow.service.exception.WorkFlowException;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class HistoryProcessServiceImpl implements HistoryProcessService {
    @Resource
    HistoryService historyService;


    /**
     * jy : 查询 负责人的 已完成的历史任务
     *
     * @param assignee 负责人
     * @return List<TaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<HistoryTaskQueryDTO> queryAssigneeFinishHistoryTask(String processDefinitionKey, String assignee) {
        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                .processFinished()
                .processDefinitionKey(processDefinitionKey)
                .taskAssignee(assignee)
                .orderByHistoricTaskInstanceEndTime().desc()
                .list();

        List<HistoryTaskQueryDTO> historyTaskQueryDTOS;
        try {
            historyTaskQueryDTOS = changeInstanceObjToDTO(historicTaskInstances);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_DTO_LIST_CHANGE_EXCEPTION, e);
        }
        return historyTaskQueryDTOS;
    }

    /**
     * jy : 查询 负责人的 未完成的历史任务
     *
     * @param assignee 负责人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<HistoryTaskQueryDTO> queryAssigneeUnFinishHistoryTask(String processDefinitionKey, String assignee) {
        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                .processUnfinished()
                .processDefinitionKey(processDefinitionKey)
                .taskAssignee(assignee)
                .orderByHistoricTaskInstanceEndTime().desc()
                .list();

        List<HistoryTaskQueryDTO> historyTaskQueryDTOS = null;
        try {
            historyTaskQueryDTOS = changeInstanceObjToDTO(historicTaskInstances);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        return historyTaskQueryDTOS;
    }

    /**
     * jy : 查询 候选人的 已完成的历史任务
     *
     * @param candidate 候选人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<HistoryTaskQueryDTO> queryCandidateFinishHistoryTask(String processDefinitionKey, String candidate) {
        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                .processFinished()
                .processDefinitionKey(processDefinitionKey)
                .taskCandidateUser(candidate)
                .orderByHistoricTaskInstanceEndTime().desc()
                .list();

        List<HistoryTaskQueryDTO> historyTaskQueryDTOS;
        try {
            historyTaskQueryDTOS = changeInstanceObjToDTO(historicTaskInstances);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        return historyTaskQueryDTOS;
    }

    /**
     * jy : 查询 负责人的 未完成的历史任务
     *
     * @param candidate 候选人
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<HistoryTaskQueryDTO> queryCandidateUnFinishHistoryTask(String processDefinitionKey, String candidate) {
        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                .processUnfinished()
                .processDefinitionKey(processDefinitionKey)
                .taskCandidateUser(candidate)
                .orderByHistoricTaskInstanceEndTime().desc()
                .list();

        List<HistoryTaskQueryDTO> historyTaskQueryDTOS;
        try {
            historyTaskQueryDTOS = changeInstanceObjToDTO(historicTaskInstances);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        return historyTaskQueryDTOS;
    }

    /**
     * 特殊方法 :  把 HistoricTaskInstance 转化为 DTO
     * @param instanceList instanceList 实例的 list
     * @return List<HistoryTaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @Override
    public List<HistoryTaskQueryDTO> changeInstanceObjToDTO(List<HistoricTaskInstance> instanceList) {

        List<HistoryTaskQueryDTO> dtoList;
        try {
            dtoList = new ArrayList<>();

            for (HistoricTaskInstance historicTaskInstance : instanceList) {
                HistoryTaskQueryDTO historyTaskQueryDTO = new HistoryTaskQueryDTO();
                historyTaskQueryDTO.setBusinessKey(historyService.createHistoricProcessInstanceQuery().processInstanceId(historicTaskInstance.getProcessInstanceId()).singleResult().getBusinessKey());
                historyTaskQueryDTO.setName(historicTaskInstance.getName());
                historyTaskQueryDTO.setTaskId(historicTaskInstance.getId());
                dtoList.add(historyTaskQueryDTO);
            }
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION, e);
        }
        return dtoList;
    }
}
