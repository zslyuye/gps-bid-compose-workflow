package com.sd365.workflow.service.exception;

import com.sd365.common.core.common.exception.code.IErrorCode;

public enum WorkFlowException implements IErrorCode {

    GPA_WORKFLOW_DEPLOY_PROCESS_EXCEPTION(1000010, "工作流部署流程异常"),
    GPA_WORKFLOW_START_PROCESS_INSTANCE_EXCEPTION(1000011, "工作流开始流程实例异常"),
    GPA_WORKFLOW_DELETE_PROCESS_INSTANCE_EXCEPTION(1000012, "工作流删除流程实例异常"),
    GPA_WORKFLOW_FIND_CANDIDATE_TASK_EXCEPTION(1000013, "工作流查询候选人任务异常"),
    GPA_WORKFLOW_GENERAL_EXCEPTION(1000016, "工作流异常"),
    GPA_WORKFLOW_COMPILE_TASK_FAIL_EXCEPTION(1000014, "工作流完成任务失败"),
    GPA_WORKFLOW_CLAIM_TASK_FAIL_EXCEPTION(1000015, "工作流拾取任务异常"),
    GPA_WORKFLOW_SELECT_CANDIDATE_ERROR(100001, "工作流查询候选人异常"),

    GPA_WORKFLOW_PROCESS_INSTANCE_IS_NULL_EXCEPTION(100002,"流程实例对象为null异常"),

    GPA_WORKFLOW_BUSINESS_GET_EXCEPTION(1000017,"工作流业务id获取异常"),
    GPA_WORKFLOW_DTO_LIST_CHANGE_EXCEPTION(1000018,"工作流dotList拷贝异常"),

    GPA_WORKFLOW_HISTORY_QUERY_EXCEPTION(1000019,"查询历史任务异常");
    /**
     * 错误码
     */
    private int code;
    /**
     * 错误消息
     */
    private String message;

    WorkFlowException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
