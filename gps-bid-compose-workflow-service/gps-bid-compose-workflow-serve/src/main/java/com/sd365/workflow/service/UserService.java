package com.sd365.workflow.service;

import java.util.List;
import java.util.Map;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:20
 */
public interface UserService {
    /**
     * 根据用户 id 查询用户的组
     *
     * @param id 用户 id
     * @return List<String> 用户组列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<String> queryGroupByUserId(String id);

    Map<String, Object> queryVariables(String taskDefinitionKey, String assignee);

    /**
     * 通过 区划， 单位， 角色， 定位候选人组
     * @param zone 区划
     * @param unit 单位
     * @param role 角色
     * @return 定位候选人组
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<String> getCandidateByZoneUnitRole(String zone, String unit, String role);


    /**
     * Moke DATA
     * 查询政府采购的候选人
     * 通过 区划， 单位， 角色， 定位候选人组
     * @param zone 区划
     * @param unit 单位
     * @param role 角色
     * @return 定位候选人组
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<String> getCandidateWithGovernment(String zone, String unit, String role);

    /**
     * Moke DATA
     * 查询代理机构的候选人
     * 通过 区划， 单位， 角色， 定位候选人组
     * @param zone 区划
     * @param unit 单位
     * @param role 角色
     * @return 定位候选人组
     * @author jy
     * @date 13:57 2020/12/30
     */
    List<String> getCandidateWithAgent(String zone, String unit, String role);
}
