package com.sd365.workflow.service.impl;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.workflow.pojo.dto.TaskQueryDTO;
import com.sd365.workflow.service.ProcessService;
import com.sd365.workflow.service.exception.WorkFlowException;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:19
 */
@Slf4j
@Service
public class ProcessServiceImpl implements ProcessService {
    /* 流程引擎 */
    ProcessEngine processEngine;
    /* 存储服务 */
    RepositoryService repositoryService;
    /* 运行时服务 */
    RuntimeService runtimeService;
    /* 任务服务 */
    TaskService taskService;

    @Autowired
    public ProcessServiceImpl(ProcessEngine processEngine,
                              RepositoryService repositoryService,
                              RuntimeService runtimeService,
                              TaskService taskService) {
        this.processEngine = processEngine;
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
    }

    /**
     * 流程部署
     *
     * @param processName 流程名
     * @param resource    流程bpmn文件路径
     * @return void
     * @author jy
     * @date 13:57 2020/12/30
     */
    public Boolean deployment(String processName, String resource) {
        // 使用 RepositoryService 流程文件部署到数据库中
        Deployment deploy = repositoryService.createDeployment()
                .enableDuplicateFiltering()  // 开启 重复部署 过滤， 如果出现 一样字段的 部署实例的话，就不会重复部署
                .name(processName)
                .addClasspathResource(resource)
                .deploy();
        return deploy != null;
    }

    /**
     * 流程启动服务
     *
     * @param processDefinitionKey 流程定义 key
     * @param businessKey          业务标识 key
     * @param assignee             启动流程用户
     * @param status               执行的状态（退回或者前进）
     * @return boolean 成功返回 true
     * @author jy
     * @date 21:29 2021/1/13
     */
    @Override
    public ProcessInstance start(String processDefinitionKey, String businessKey, String assignee, String zone, String unit, Integer status) {
        System.out.println(processDefinitionKey + "+" + businessKey + "+" + assignee);
        // 流程启动参数 Map ——> variables
        Map<String, Object> variables = new HashMap<>();
        // 启动流程的用户为当前传入的用户
        variables.put("zone", zone); // 区划：指当前的流程发起人的区划
        variables.put("unit", unit); // 单位：指当前的流程发起人的单位
        variables.put("startUser", assignee); // 指当前的流程发起人
        variables.put("status", status); // 执行的状态（退回或者前进）
        // 启动流程
        ProcessInstance processInstance;
        try {
            processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey, variables);
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_START_PROCESS_INSTANCE_EXCEPTION, e);
        }
        return processInstance;
    }

    /**
     * 根据流程定义 key 和 用户 id 查询对应用户对应流程代办列表
     *
     * @param processDefinitionKey 流程定义 key
     * @param candidateUser        候选人
     * @return List<Task> 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    public List<TaskQueryDTO> queryTask(String processDefinitionKey, String taskDefinitionKey, String candidateUser) {
        //查询组任务
        List<Task> taskList = taskService.createTaskQuery()
                .processDefinitionKey(processDefinitionKey)
                .taskDefinitionKey(taskDefinitionKey)
                .taskCandidateUser(candidateUser)//根据候选人查询
                .list();
        List<TaskQueryDTO> taskQueryDTOList = new ArrayList<>();
        // 通过 task 的 流程实例 id 获取 businessKey
        try {
            for (Task task : taskList) {
                log.info("----------------------------");
                log.info("流程实例id：" + task.getProcessInstanceId());
                log.info("任务id：" + task.getId());
                log.info("任务负责人：" + task.getAssignee());
                log.info("任务名称：" + task.getName());
                TaskQueryDTO taskQueryDTO = new TaskQueryDTO();
                // 添加任务 id
                taskQueryDTO.setTaskId(task.getId());
                // 添加 任务名称
                taskQueryDTO.setName(task.getName());
                // 添加状态
                taskQueryDTO.setStatus("正常前进");
                // 添加 任务 的 key
                taskQueryDTO.setTaskDefinitionKey(task.getTaskDefinitionKey());
                // 添加 businessKey
                taskQueryDTO.setBusinessKey(runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult().getBusinessKey());
                taskQueryDTOList.add(taskQueryDTO);
            }
        } catch (Exception e) {
            // 防止 business 为 null
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_BUSINESS_GET_EXCEPTION, e);
        }
        return taskQueryDTOList;
    }

    /**
     * 完成任务
     *
     * @param taskId   任务 id
     * @param assignee 负责人
     * @return boolean
     * @author jy
     * @date 13:57 2020/12/30
     */
    public Boolean completeTask(String taskId, String assignee, Integer status) {
        Task task = taskService.createTaskQuery().
                taskId(taskId).
                singleResult();

        log.info("----------------------------------");
        log.info("完成任务， task 为 " + task);
        // 如果有任务， 就设置
        try {
            if (task != null) {
                taskService.setAssignee(task.getId(), assignee);
                HashMap<String, Object> map = new HashMap<>();
                map.put("status", status);
                taskService.complete(task.getId(), map);
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_COMPILE_TASK_FAIL_EXCEPTION, e);
        }
    }

    /**
     * 拾取任务
     *
     * @param candidateUser 候选人
     * @param taskId        任务 id
     * @return boolean 成功返回 true
     * @author jy
     * @date 13:57 2020/12/30
     */
    public Boolean claimTask(String taskId, String candidateUser) {
        // 校验该用户是否有资格拾取任务
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskCandidateUser(candidateUser)//根据候选人查询
                .singleResult();
        try {
            if (task != null) {
                // 拾取任务
                taskService.claim(taskId, candidateUser);
                log.info(task.getName());
                log.info(task.getId());
                log.info(task.getAssignee());
                return true;
            }
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_CLAIM_TASK_FAIL_EXCEPTION, e);
        }
        return false;
    }


    /**
     * jy : 删除  部署 的 流程
     *
     * @param processInstanceId 流程实例 id
     * @return 成功 true， 失败 false
     * @author jy
     * @date 13:57 2020/12/30
     */
    public Boolean deleteDeployProcess(String processInstanceId) {
        ProcessDefinition processDefinition;
        // 删除流程
        try {
            // 获取流程 对象
            processDefinition = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionKey(processInstanceId)
                    .singleResult();
            if (processDefinition == null) {
                throw new BusinessException(WorkFlowException.GPA_WORKFLOW_PROCESS_INSTANCE_IS_NULL_EXCEPTION, new Exception());
            }
            // 获取部署id
            String deploymentId = processDefinition.getDeploymentId();
            // 级联删除 部署的 流程
            repositoryService.deleteDeployment(deploymentId, true);
            return true;
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_DELETE_PROCESS_INSTANCE_EXCEPTION, e);
        }
    }


    /**
     * 根据流程定义 key 和 用户 id 查询对应  负责人 对应流程代办列表
     *
     * @param processDefinitionKey 流程定义 key
     * @param assignee             负责人
     * @return List<Task> 任务列表
     * @author jy
     * @date 21:51 2020/12/28
     */
    @Override
    public List<TaskQueryDTO> queryAssigneeTask(String processDefinitionKey, String taskDefinitionKey, String assignee) {
        List<Task> taskList;
        try {
            //查询组任务
            taskList = taskService.createTaskQuery()
                    .processDefinitionKey(processDefinitionKey)  // 指定 流程 的 key， 保证不会查询错流程
                    .taskDefinitionKey(taskDefinitionKey)   // 指定 任务 的 key ，保证不会 在不同的阶段访问到不属于当前阶段的任务
                    .taskAssignee(assignee)//根据候选人查询
                    .list();
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_FIND_CANDIDATE_TASK_EXCEPTION, e);
        }
        List<TaskQueryDTO> taskQueryDTOS = new ArrayList<>();
        // 通过 task 的 流程实例 id 获取 businessKey
        try {
            for (Task task : taskList) {
                log.info("----------------------------");
                log.info("流程实例id：" + task.getProcessInstanceId());
                log.info("任务id：" + task.getId());
                log.info("任务负责人：" + task.getAssignee());
                log.info("任务名称：" + task.getName());
                TaskQueryDTO taskQueryDTO = new TaskQueryDTO();
                // 设置用于返回 对象 的属性
                taskQueryDTO.setTaskId(task.getId());
                // 添加 任务名称
                taskQueryDTO.setName(task.getName());
                // 添加状态
                taskQueryDTO.setStatus("正常前进");
                // 添加 任务 的 key
                taskQueryDTO.setTaskDefinitionKey(task.getTaskDefinitionKey());
                // 添加 businessKey
                taskQueryDTO.setBusinessKey(runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult().getBusinessKey());
                taskQueryDTOS.add(taskQueryDTO);
            }
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_BUSINESS_GET_EXCEPTION, e);
        }
        return taskQueryDTOS;
    }
}
