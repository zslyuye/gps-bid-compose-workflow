package com.sd365.workflow.api;

import com.sd365.workflow.pojo.vo.HistoryTaskQueryVO;
import com.sd365.workflow.pojo.vo.TaskQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 政府采购工作流 历史 api
 *
 * @author jy
 * @date 2021/1/13
 */
@Api(tags = "工作流历史记录管理", value = "/gps/workflow/history")
@CrossOrigin
@RequestMapping(value = "/gps/workflow/history")
public interface HistoryApi {
    /**
     * jy 查询当前负责人已完成的历史任务
     * @param assignee 负责人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @GetMapping("/assignee/on")
    @ApiOperation(tags = "查询当前负责人已完成的历史任务", value = "/assignee/on")
    @ResponseBody
    List<HistoryTaskQueryVO> queryAssigneeFinishHistoryTask(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                                            @RequestParam("assignee") String assignee);


    /**
     * jy 查询当前负责人未完成的历史任务
     * @param assignee 负责人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @GetMapping("/assignee/off")
    @ApiOperation(tags = "查询当前负责人未完成的历史任务", value = "/assignee/off")
    @ResponseBody
    List<HistoryTaskQueryVO> queryAssigneeUnFinishHistoryTask(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                                              @RequestParam("assignee") String assignee);


    /**
     * jy 查询候选人 的 历史任务
     * @param candidateUser 候选人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @GetMapping("/candidate/on")
    @ApiOperation(tags = "查询当前候选人已完成的历史任务", value = "/candidate/on")
    @ResponseBody
    List<HistoryTaskQueryVO> queryCandidateFinishHistoryTask(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                                             @RequestParam("candidateUser") String candidateUser);


    /**
     * jy 查询候选人 的 历史任务
     * @param candidateUser 候选人
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @GetMapping("/candidate/off")
    @ApiOperation(tags = "查询当前候选人未完成的历史任务", value = "/candidate/off")
    @ResponseBody
    List<HistoryTaskQueryVO> queryCandidateUnFinishHistoryTask(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                                               @RequestParam("candidateUser") String candidateUser);



}
