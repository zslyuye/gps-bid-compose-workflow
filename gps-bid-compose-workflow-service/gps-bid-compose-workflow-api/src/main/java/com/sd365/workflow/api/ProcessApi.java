package com.sd365.workflow.api;

import com.sd365.workflow.pojo.dto.TaskQueryDTO;
import com.sd365.workflow.pojo.vo.TaskQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * jy : 政府采购工作流 api
 *
 * @author jy
 * @date 2021/1/13
 */
@CrossOrigin
@Api(tags = "工作流流程管理", value = "/gps/workflow/process")
@RequestMapping(value = "/gps/workflow/process")
public interface ProcessApi {

    /**
     * jy : 部署流程
     *
     * @param processName  流程名称
     * @param resourcePath bpmn资源路径
     * @return 部署成功 返回 true  失败 则 返回false
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ResponseBody
    @PostMapping(value = "/deploy")
    @ApiOperation(tags = "部署流程", value = "/deploy")
    Boolean deployProcess(@RequestParam("processName") String processName,
                          @RequestParam("resourcePath") String resourcePath);

    /**
     * jy : 启动流程
     *
     * @param processDefinitionKey 启动流程 的 key， 就是 背景 的 id
     * @param businessKey          业务流程 key， 就是 计划id
     * @param assignee             负责人
     * @param unit                 单位
     * @param zone                 区划
     * @param status               状态， 》 1 就是退回
     * @return TaskQueryVO vo对象
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ResponseBody
    @GetMapping(value = "/start")
    @ApiOperation(tags = "启动流程", value = "/start")
    TaskQueryVO startProcess(@RequestParam("processDefinitionKey") String processDefinitionKey,
                             @RequestParam("businessKey") String businessKey,
                             @RequestParam("assignee") String assignee,
                             @RequestParam("unit") String unit,
                             @RequestParam("zone") String zone,
                             @RequestParam("status") Integer status);

    /**
     * jy : 查询候选人代办流程
     *
     * @param processDefinitionKey 流程定义 key
     * @param taskDefinitionKey    任务 定义key ： 任务的 id
     * @param candidateUser        候选人
     * @return List<TaskQueryDTO>
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiOperation(tags = "查询代办流程", value = "/")
    @GetMapping(value = "/")
    @ResponseBody
    List<TaskQueryDTO> queryCandidateTask(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                          @RequestParam("taskDefinitionKey") String taskDefinitionKey,
                                          @RequestParam("candidateUser") String candidateUser);


    /**
     * 查询 负责人 的 代办流程
     *
     * @param processDefinitionKey 流程定义 key ： 流程的 id
     * @param taskDefinitionKey    任务 定义key ： 任务的 id
     * @param assignee             负责人
     * @return List<TaskQueryDTO> 任务 dto 对象
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiOperation(tags = "查询代办流程", value = "/assignee")
    @GetMapping(value = "/assignee")
    @ResponseBody
    List<TaskQueryDTO> queryAssigneeTask(@RequestParam("processDefinitionKey") String processDefinitionKey,
                                         @RequestParam("taskDefinitionKey") String taskDefinitionKey,
                                         @RequestParam("assignee") String assignee);

    /**
     * 完成任务
     *
     * @param taskId   流程实例 id
     * @param assignee 负责人
     * @return Boolean  任务完成成功 返回 true  失败 则 返回false
     * @author jy
     * @date 13:57 2020/12/30
     */
    @ApiOperation(tags = "完成任务", value = "/")
    @PutMapping(value = "/")
    @ResponseBody
    Boolean completeTask(@RequestParam("taskId") String taskId,
                         @RequestParam("assignee") String assignee,
                         @RequestParam("status") Integer status);

    /**
     * 拾取任务
     *
     * @param taskId        任务 id
     * @param candidateUser 候选人
     * @return Boolean  拾取成功完成成功 返回 true  失败 则 返回false
     * @author jy
     * @date 14:46 2021/1/4
     */
    @ApiOperation(tags = "拾取任务", value = "/claim")
    @PostMapping(value = "/claim")
    @ResponseBody
    Boolean claimTask(@RequestParam("taskId") String taskId,
                      @RequestParam("candidateUser") String candidateUser);


    /**
     * 结束 流程
     *
     * @param processInstanceId 实例 id
     * @return Boolean  流程结束成功 返回 true  失败 则 返回false
     * @author jy
     * @date 14:46 2021/1/4
     */
    @ResponseBody
    @DeleteMapping(value = "/")
    @ApiOperation(tags = "结束 流程", value = "/")
    Boolean deleteDeployProcess(@RequestParam("processInstanceId") String processInstanceId);
}
