package com.sd365.workflow.api;

import com.sd365.workflow.pojo.vo.HistoryTaskQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 政府采购工作流 资源 api
 *
 * @author jy
 * @date 2021/1/13
 */
@Api(tags = "工作流资源管理", value = "/gps/workflow/resource")
@CrossOrigin
@RequestMapping(value = "/gps/workflow/resource")
public interface ResourceApi {
    /**
     * jy 下载 bpmn图
     * @param processDefinitionKey 流程定义key
     * @return 任务列表
     * @author jy
     * @date 13:57 2020/12/30
     */
    @GetMapping("/")
    @ApiOperation(tags = "查询当前负责人的历史任务", value = "/")
    @ResponseBody
    void getBpmn(@RequestParam("processDefinitionKey")String processDefinitionKey,
                                     @RequestParam("assignee")String assignee) throws IOException;


    @RequestMapping(value = "/download", method = RequestMethod.GET)
    String downloadData(HttpServletResponse res);

}
