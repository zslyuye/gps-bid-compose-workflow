package com.sd365.workflow.configuration;

import org.activiti.engine.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2021-01-04 13:38
 */
@Configuration
public class ActivitiConfiguration {

    @Bean
    ProcessEngine getProcessEngine() {
        ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration
                .createStandaloneProcessEngineConfiguration();
        //Jdbc设置
        String jdbcDriver = "com.mysql.jdbc.Driver";
        processEngineConfiguration.setJdbcDriver(jdbcDriver);
        String jdbcUrl = "jdbc:mysql://47.97.184.36:3306/activiti?characterEncoding=UTF-8&nullCatalogMeansCurrent=true&serverTimezone=GMT&useSSL=false";
        processEngineConfiguration.setJdbcUrl(jdbcUrl);
        String jdbcUsername = "root";
        processEngineConfiguration.setJdbcUsername(jdbcUsername);
        String jdbcPassword = "123456";
        processEngineConfiguration.setJdbcPassword(jdbcPassword);
        String databaseSchemaUpdate = "true";
        processEngineConfiguration.setDatabaseSchemaUpdate(databaseSchemaUpdate);
        // 获取引擎对象 :　建表
        return processEngineConfiguration.buildProcessEngine();
    }

    @Bean
    RepositoryService getRepositoryService(){
        return getProcessEngine().getRepositoryService();
    }

    @Bean
    TaskService getTaskService(){
        return getProcessEngine().getTaskService();
    }

    @Bean
    RuntimeService getRuntimeService(){
        return getProcessEngine().getRuntimeService();
    }

    @Bean
    HistoryService getHistoryService(){
        return getProcessEngine().getHistoryService();
    }
}
