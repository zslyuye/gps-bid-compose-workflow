import lombok.extern.slf4j.Slf4j;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Generator {
    public static InputStream getResourceAsStream(String path) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
    }

    /**
     * mybatis 逆向方法
     * @author Yan Huazhi
     * @date 11:37 2021/1/5
     * @param args 系统传入参数
     * @return void
     */
    public static void main(String[] args) throws Exception {
        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;
        ConfigurationParser cp = new ConfigurationParser(warnings);
        //read local config file in resource directory
        Configuration config = cp.parseConfiguration(getResourceAsStream("mybatis/Config.xml"));
        DefaultShellCallback callback = new DefaultShellCallback(true);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        // generat code in pre-define file which on resource/mybatis/generator-config.xml
        myBatisGenerator.generate(null);
        for (String warning : warnings) {
            log.info(warning);
        }
    }

}
