package com.sd365.workflow.dao.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;


@Table(name = "basic_zone")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Zone extends BaseEntity {
    /**
     * 名称
     */
    @ApiModelProperty(value="name区划名称")
    private String name;

    /**
     * 编号
     */
    @ApiModelProperty(value="code编号")
    private String code;

    /**
     * 负责人
     */
    @ApiModelProperty(value="master负责人")
    private String master;

    /**
     * 手机号
     */
    @ApiModelProperty(value="tel手机号")
    private String tel;

    /**
     * 地址
     */
    @ApiModelProperty(value="address地址")
    private String address;
}
