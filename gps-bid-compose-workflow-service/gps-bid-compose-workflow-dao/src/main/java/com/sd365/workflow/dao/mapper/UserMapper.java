package com.sd365.workflow.dao.mapper;
import com.sd365.common.core.common.dao.CommonMapper;

import com.sd365.workflow.dao.entity.Role;
import com.sd365.workflow.dao.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper extends CommonMapper<User> {
    /**
     * 通过 unit zone role 定位 所有 候选人
     * @param zoneCode
     * @param roleCode
     * @param unitCode
     * @return
     */
    List<User> selectCandidateByZoneUnitRole(String zoneCode, String unitCode, String roleCode);

}