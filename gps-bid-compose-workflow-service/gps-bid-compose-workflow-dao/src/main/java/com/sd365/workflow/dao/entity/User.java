package com.sd365.workflow.dao.entity;

import lombok.Data;


/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2021-01-05 14:33
 */
@Data
public class User {
    /**
     * 代码， 不可 重复
     */
    private String code;
    /**
     * 用户名称
     */
    private String name;
    /**
     * 角色
     */
    private Role role;
    /**
     * 区划
     */
    private Zone zone;
    /**
     * 单位
     */
    private Unit unit;
}
