package com.sd365.workflow.service.listener;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.workflow.service.UserService;
import com.sd365.workflow.service.exception.WorkFlowException;
import com.sd365.workflow.service.impl.UserServiceImpl;
import com.sd365.workflow.util.SpringBeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.nio.file.Watchable;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component(value = "planListener")
public class PlanListener implements TaskListener {

    private Expression role;

    @Resource
    private UserService userService;

    @Override
    public void notify(DelegateTask delegateTask) {
        // 手动加载注入 userService
        userService = SpringBeanUtil.getObject(UserServiceImpl.class);
        log.info("roleCode : " + role.getValue(delegateTask).toString());
        String roleCode = role.getValue(delegateTask).toString();
        // 获取 流程创建时加入的 zoneCode 和 unitCode
        String zone = (String) delegateTask.getVariable("zone");
        String unit = (String) delegateTask.getVariable("unit");
        List<String> candidateList = null;
        // 判断 如果 role 是政府 部门的 人员， 就设置政府的list
        try {
            if (roleCode.equals("007")) {
                //过userService 查询出当前节点的候选人
                log.info("政府部门 : " + zone + " - " + unit + " - " + roleCode);
                candidateList = userService.getCandidateByZoneUnitRole(zone, unit, roleCode);
            } else if (roleCode.equals("00001")){
                // 是 00001 就设置成代理机构的
                log.info("代理机构 : " + zone + " - " + unit + " - " + roleCode);
                candidateList = userService.getCandidateByZoneUnitRole(zone, unit, roleCode);
            }
        } catch (Exception e) {
            throw new BusinessException(WorkFlowException.GPA_WORKFLOW_SELECT_CANDIDATE_ERROR, e);
        }
        // 设置候选人组
        delegateTask.addCandidateUsers(candidateList);
    }
}
