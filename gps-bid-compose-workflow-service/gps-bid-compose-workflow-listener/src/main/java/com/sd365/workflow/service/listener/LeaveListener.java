package com.sd365.workflow.service.listener;

import com.sd365.workflow.service.impl.UserServiceImpl;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Yan Huazhi
 * @version 0.0.1
 * @date 2020-12-30 23:29
 */
@Component
public class LeaveListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        if (delegateTask.getName().equals("创建请假申请")) {
            String taskDefinitionKey = delegateTask.getTaskDefinitionKey();
            String assignee = delegateTask.getAssignee();
            UserServiceImpl userService = new UserServiceImpl();
            // 查询相应的参数
            Map<String, Object> variables = userService.queryVariables(taskDefinitionKey, assignee);
            System.out.println("参数:"+assignee);
            System.out.println("组："+variables.get("group"));
//            // 添加参数
            delegateTask.setVariables(variables);
        }
//        else if (delegateTask.getName().equals("创建请假申请")) {
//            String taskDefinitionKey = delegateTask.getTaskDefinitionKey();
//            String assignee = delegateTask.getAssignee();
//            UserServiceImpl userService = new UserServiceImpl();
//            // 查询相应的参数
//            Map<String, Object> variables = userService.queryVariables(taskDefinitionKey, assignee);
//            System.out.println("参数create"+assignee);
////            // 添加参数
//            delegateTask.setVariables(variables);
//        }
    }
}
